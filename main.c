#include <stdio.h>
#include <stdint.h>

#define LOCAL
#define GLOBAL

typedef char CHAR;                      ///< tipo di dato "char"
typedef unsigned char U8;               ///< 8-bits unsigned [0..255]
typedef signed char S8;                 ///< 8-bits signed [-128..127]
typedef unsigned short U16;             ///< 16-bits unsigned [0..65535]
typedef signed short S16;               ///< 16-bits signed [-32768..32767]
typedef unsigned long U32;              ///< 32-bits unsigned [0..2^32 - 1]
typedef signed long S32;                ///< 32-bits signed [-2^31..2^31 - 1]
typedef unsigned long long U64;         ///< 64-bits unsigned [0..2^64 - 1] added with C99
typedef signed long long S64;           ///< 64-bits signed [-2^64..2^64 - 1] added with C99
typedef float FLOAT;                    ///< 32-bits floating [±1.18E-38 to ±3.40E+38]
typedef double DOUBLE;                  ///< 64-bits double [±2.23E-308 to ±1.79E+308]


#define	DPT_KNX_FLOAT_M_BIT		11		///< numero bit della mantissa di un DPT_KNX_FLOAT (9.0xx)
#define	DPT_KNX_FLOAT_E_BIT		4		///< numero bit dell'esponente di un DPT_KNX_FLOAT (9.0xx)
#define	DPT_KNX_FLOAT_BIT		16		///< numero di bit che occupa un DPT_KNX_FLOAT (9.0xx)
#define DPT_KNX_FLOAT_E_MASK		(0x78)
#define DPT_KNX_FLOAT_M_MASK		(0x07FF)
#define DPT_KNX_FLOAT_S_MASK		(0x8000)
#define DPT_KNX_FLOAT_M_MAX		(2048)


#pragma pack(1)
typedef union {
	struct {
		uint16_t m :11;
		uint16_t e :4;
		uint16_t s :1;
	};
	uint16_t raw;
} KNX_FLOAT_TYPE;
#pragma pack()




GLOBAL FLOAT BYME_FromKnxFloat_F(KNX_FLOAT_TYPE fKnxValue) {

	U8 u8S = 0;
	S32 s32ValueCoverted = 0;

	if (fKnxValue.s) {
		u8S = 1;
		// prendo il complemento a 1
		fKnxValue.m = (~(fKnxValue.m) + 1);
	}
	s32ValueCoverted = (S32) (fKnxValue.m);
	if ((s32ValueCoverted == 0) && (u8S == 1)) {
		// prendo il complemento a 1 se 0 gestito a parte
		s32ValueCoverted = DPT_KNX_FLOAT_M_MAX;
	}

	while (fKnxValue.e) {
		// riscalo mantissa
		s32ValueCoverted <<= 1;
		fKnxValue.e--;
	}

	if (u8S) {
		s32ValueCoverted = -s32ValueCoverted;
	}

	FLOAT returnValue = ((FLOAT) (s32ValueCoverted) / (100.0));

	return returnValue;
}


int main()
{
    KNX_FLOAT_TYPE knx;
    int in;
    float out;

    printf ("\n");
    printf ("*** Starting ***\n");
    while(1)
      {
        printf ("Enter a KNX FLOAT hexadecimal number: \n");
        scanf  ("%x", &in);

        knx.raw = in;
        out = BYME_FromKnxFloat_F(knx);

        printf ("\n");

        printf("   in  = 0x%x\n", knx.raw);
        printf("-> out = %f \n", out);

        printf ("\n");
      }

    return 0;
}

